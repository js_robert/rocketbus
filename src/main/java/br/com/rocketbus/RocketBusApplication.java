package br.com.rocketbus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RocketBusApplication {

	public static void main(String[] args) {
		SpringApplication.run(RocketBusApplication.class, args);
	}

}
